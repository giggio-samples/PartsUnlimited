﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Mvc;
using PartsUnlimited.Search;

namespace PartsUnlimited.Controllers
{
    public class SearchController(IProductSearch search) : Controller
    {
        public async Task<IActionResult> Index(string q)
        {
            if (string.IsNullOrWhiteSpace(q))
            {
                return View(null);
            }

            var result = await search.Search(q);

            return View(result);
        }
    }
}
