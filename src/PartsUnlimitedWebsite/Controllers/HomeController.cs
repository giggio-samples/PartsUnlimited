// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using PartsUnlimited.Models;
using PartsUnlimited.ViewModels;

namespace PartsUnlimited.Controllers
{
    public class HomeController(IPartsUnlimitedContext context, IMemoryCache memoryCache) : Controller
    {
        public int roco_count = 1000;

        //
        // GET: /Home/
        public IActionResult Index()
        {
            // Get most popular products
            if (!memoryCache.TryGetValue("topselling", out List<Product> topSellingProducts))
            {
                topSellingProducts = GetTopSellingProducts(4);
                //Refresh it every 10 minutes. Let this be the last item to be removed by cache if cache GC kicks in.
                memoryCache.Set("topselling", topSellingProducts, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(10)).SetPriority(CacheItemPriority.High));
            }

            if (!memoryCache.TryGetValue("newarrivals", out List<Product> newProducts))
            {
                newProducts = GetNewProducts(4);
                //Refresh it every 10 minutes. Let this be the last item to be removed by cache if cache GC kicks in.
                memoryCache.Set("newarrivals", newProducts, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(10)).SetPriority(CacheItemPriority.High));
            }

            var viewModel = new HomeViewModel
            {
                NewProducts = newProducts,
                TopSellingProducts = topSellingProducts,
                CommunityPosts = GetCommunityPosts()
            };

            return View(viewModel);
        }

        //Can be removed and handled when HandleError filter is implemented
        //https://github.com/aspnet/Mvc/issues/623
        public IActionResult Error()
        {
            return View("~/Views/Shared/Error.cshtml");
        }

        private List<Product> GetTopSellingProducts(int count)
        {
            // Group the order details by product and return
            // the products with the highest count

            // TODO [EF] We don't query related data as yet, so the OrderByDescending isn't doing anything
            return [.. context.Products
                .OrderByDescending(a => a.OrderDetails.Count())
                .Take(count)];
        }

        private List<Product> GetNewProducts(int count)
        {
            return [.. context.Products
                .OrderByDescending(a => a.Created)
                .Take(count)];
        }

        private static List<CommunityPost> GetCommunityPosts()
        {
            return [
                new CommunityPost {
                    Content= "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus commodo tellus lorem, et bibendum velit sagittis in. Integer nisl augue, cursus id tellus in, sodales porta.",
                    DatePosted = DateTime.Now,
                    Image = "community_1.png",
                    Source = CommunitySource.Facebook
                },
                new CommunityPost {
                    Content= " Donec tincidunt risus in ligula varius, feugiat placerat nisi condimentum. Quisque rutrum eleifend venenatis. Phasellus a hendrerit urna. Cras arcu leo, hendrerit vel mollis nec.",
                    DatePosted = DateTime.Now,
                    Image = "community_2.png",
                    Source = CommunitySource.Facebook
                },
                new CommunityPost {
                    Content= "Aenean vestibulum non lacus non molestie. Curabitur maximus interdum magna, ullamcorper facilisis tellus fermentum eu. Pellentesque iaculis enim ac vestibulum mollis.",
                    DatePosted = DateTime.Now,
                    Image = "community_3.png",
                    Source = CommunitySource.Facebook
                },
                new CommunityPost {
                    Content= "Ut consectetur sed justo vel convallis. Vestibulum quis metus leo. Nulla hendrerit pharetra dui, vel euismod lectus elementum sit amet. Nam dolor turpis, sodales non mi nec.",
                    DatePosted = DateTime.Now,
                    Image = "community_4.png",
                    Source = CommunitySource.Facebook
                }
            ];
        }


        //stubbing in a recommendations action
        public ActionResult Recommendations()
        {
            var count = 0;
            while ( count < roco_count)
            {
                _ = context.Products
                    .OrderByDescending(a => a.OrderDetails.Count())
                    .Take(count++)
                    .ToList();
            }

            return View();
        }
    }
}
