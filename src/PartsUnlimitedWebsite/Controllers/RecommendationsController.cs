﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PartsUnlimited.Models;
using PartsUnlimited.Recommendations;
using PartsUnlimited.WebsiteConfiguration;

namespace PartsUnlimited.Controllers
{
    public class RecommendationsController(IPartsUnlimitedContext context, IRecommendationEngine recommendationEngine, IWebsiteOptions websiteOptions) : Controller
    {
        public async Task<IActionResult> GetRecommendations(string recommendationId)
        {
            if (!websiteOptions.ShowRecommendations)
            {
                return new EmptyResult();
            }

            var recommendedProductIds = await recommendationEngine.GetRecommendationsAsync(recommendationId);

            var productTasks = recommendedProductIds
                .Select(item => context.Products.SingleOrDefaultAsync(c => c.RecommendationId == Convert.ToInt32(item)))
                .ToList();

            await Task.WhenAll(productTasks);

            var recommendedProducts = productTasks
                .Select(p => p.Result)
                .Where(p => p != null && p.RecommendationId != Convert.ToInt32(recommendationId))
                .ToList();

            return PartialView("_Recommendations", recommendedProducts);
        }
    }
}