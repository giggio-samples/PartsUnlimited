﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Mvc;
using PartsUnlimited.Models;

namespace PartsUnlimited.Controllers
{
    public class StoreController(IPartsUnlimitedContext context) : Controller
    {

        //
        // GET: /Store/

        public IActionResult Index()
        {
            var category = context.Categories.ToList();

            return View(category);
        }

        //
        // GET: /Store/Browse?category=Brakes

        public IActionResult Browse(int categoryId)
        {
            // Retrieve Category category and its Associated associated Products products from database

            // TODO [EF] Swap to native support for loading related data when available
            var categoryModel = context.Categories.Single(g => g.CategoryId == categoryId);
            categoryModel.Products = [.. context.Products.Where(a => a.CategoryId == categoryModel.CategoryId)];

            return View(categoryModel);
        }

        public IActionResult Details(int id)
        {
            Product productData;

            productData = context.Products.Single(a => a.ProductId == id);
            productData.Category = context.Categories.Single(g => g.CategoryId == productData.CategoryId);


            return View(productData);
        }
    }
}