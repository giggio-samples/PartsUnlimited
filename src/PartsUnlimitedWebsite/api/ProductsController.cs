﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PartsUnlimited.Models;

namespace PartsUnlimited.api.Controllers
{
    [Route("api/[controller]")]
    public class ProductsController(IPartsUnlimitedContext context) : Controller
    {
        [HttpGet]
        public IEnumerable<Product> Get(bool sale = false)
        {
            if (!sale)
            {
                return context.Products;
            }

            return context.Products.Where(p => p.Price != p.SalePrice);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var product = await context.Products.FirstOrDefaultAsync(p => p.ProductId == id);

            if (product == null)
            {
                return NotFound();
            }

            return new ObjectResult(product);
        }
    }
}
