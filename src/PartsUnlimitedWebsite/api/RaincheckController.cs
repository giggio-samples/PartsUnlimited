﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Mvc;
using PartsUnlimited.Models;
using PartsUnlimited.Queries;

namespace PartsUnlimited.WebApi
{
    [Route("api/[controller]")]
    public class RaincheckController(IRaincheckQuery query) : Controller
    {
        [HttpGet]
        public Task<IEnumerable<Raincheck>> Get()
        {
            return query.GetAllAsync();
        }

        [HttpGet("{id}")]
        public Task<Raincheck> Get(int id)
        {
            return query.FindAsync(id);
        }

        [HttpPost]
        public Task<int> Post([FromBody]Raincheck raincheck)
        {
            return query.AddAsync(raincheck);
        }
    }
}
