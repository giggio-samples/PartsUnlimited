﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using PartsUnlimited.Models;
//using Microsoft.AspNetCore.Server.Kestrel.Internal.Http;

namespace PartsUnlimited.Areas.Admin.Controllers
{
    public enum SortField { Name, Title, Price }
    public enum SortDirection { Up, Down }

    public class StoreManagerController(IPartsUnlimitedContext context, IMemoryCache memoryCache) : AdminController
    {

        //
        // GET: /StoreManager/

        public IActionResult Index(SortField sortField = SortField.Name, SortDirection sortDirection = SortDirection.Up)
        {
            // TODO [EF] Swap to native support for loading related data when available
            var products = from product in context.Products
                           join category in context.Categories on product.CategoryId equals category.CategoryId
                           select new Product()
                           {
                               ProductArtUrl = product.ProductArtUrl,
                               ProductId = product.ProductId,
                               CategoryId = product.CategoryId,
                               Price = product.Price,
                               Title = product.Title,
                               Category = new Category()
                               {
                                   CategoryId = product.CategoryId,
                                   Name = category.Name
                               }
                           };

            var sorted = Sort(products, sortField, sortDirection);

            return View(sorted);
        }

        private IQueryable<Product> Sort(IQueryable<Product> products, SortField sortField, SortDirection sortDirection)
        {
            if (sortField == SortField.Name)
            {
                if (sortDirection == SortDirection.Up)
                {
                    return products.OrderBy(o => o.Category.Name);
                }
                else
                {
                    return products.OrderByDescending(o => o.Category.Name);
                }
            }

            if (sortField == SortField.Price)
            {
                if (sortDirection == SortDirection.Up)
                {
                    return products.OrderBy(o => o.Price);
                }
                else
                {
                    return products.OrderByDescending(o => o.Price);
                }
            }

            if (sortField == SortField.Title)
            {
                if (sortDirection == SortDirection.Up)
                {
                    return products.OrderBy(o => o.Title);
                }
                else
                {
                    return products.OrderByDescending(o => o.Title);
                }
            }

            // Should not reach here, but return products for compiler
            return products;
        }


        //
        // GET: /StoreManager/Details/5

        public IActionResult Details(int id)
        {
            var cacheId = string.Format("product_{0}", id);

            if (!memoryCache.TryGetValue(cacheId, out Product product))
            {
                //If this returns null, don't stick it in the cache
                product =  context.Products.FirstOrDefault(a => a.ProductId == id);

                if (product != null)
                {
                    //                               Remove it from cache if not retrieved in last 10 minutes
                    memoryCache.Set(cacheId, product, new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(10)));
                }
            }

            if (product == null)
            {
                memoryCache.Remove(cacheId);
                return View((Product)null);
            }

            // TODO [EF] We don't query related data as yet. We have to populate this until we do automatically.
            product.Category = context.Categories.Single(g => g.CategoryId == product.CategoryId);
            return View(product);
        }

        //
        // GET: /StoreManager/Create
        public IActionResult Create()
        {
            ViewBag.Categories = new SelectList(context.Categories, "CategoryId", "Name");
            return View();
        }

        // POST: /StoreManager/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Product product)
        {
            if (ModelState.IsValid)
            {
                context.Products.Add(product);
                await context.SaveChangesAsync(HttpContext.RequestAborted);
                memoryCache.Remove("announcementProduct");
                return RedirectToAction("Index");
            }

            ViewBag.Categories = new SelectList(context.Categories, "CategoryId", "Name", product.CategoryId);
            return View(product);
        }

        //
        // GET: /StoreManager/Edit/5
        public IActionResult Edit(int id)
        {
            var product = context.Products.FirstOrDefault(a => a.ProductId == id);
            ViewBag.Categories = new SelectList(context.Categories, "CategoryId", "Name", product.CategoryId).ToList();

            return View(product);
        }

        //
        // POST: /StoreManager/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Product product)
        {
            if (ModelState.IsValid)
            {
                context.Entry(product).State = EntityState.Modified;
                await context.SaveChangesAsync(HttpContext.RequestAborted);
                //Invalidate the cache entry as it is modified
                memoryCache.Remove(string.Format("product_{0}", product.ProductId));
                return RedirectToAction("Index");
            }

            ViewBag.Categories = new SelectList(context.Categories, "CategoryId", "Name", product.CategoryId);
            return View(product);
        }

        //
        // GET: /StoreManager/RemoveProduct/5
        public IActionResult RemoveProduct(int id)
        {
            var product = context.Products.FirstOrDefault(a => a.ProductId == id);
            return View(product);
        }

        //
        // POST: /StoreManager/RemoveProduct/5
        [HttpPost, ActionName("RemoveProduct")]
        public async Task<IActionResult> RemoveProductConfirmed(int id)
        {
            var product = context.Products.FirstOrDefault(a => a.ProductId == id);
            var cartItem = context.CartItems.FirstOrDefault(a => a.ProductId == id);
            List<OrderDetail> orderDetail = [.. context.OrderDetails.Where(a => a.ProductId == id)];
            List<Raincheck> rainCheck = [.. context.RainChecks.Where(a => a.ProductId == id)];

            if (product != null)
            {
                if (cartItem != null)
                {
                    context.CartItems.Remove(cartItem);
                    await context.SaveChangesAsync(HttpContext.RequestAborted);
                }

                context.OrderDetails.RemoveRange(orderDetail);
                await context.SaveChangesAsync(HttpContext.RequestAborted);

                context.RainChecks.RemoveRange(rainCheck);
                await context.SaveChangesAsync(HttpContext.RequestAborted);

                context.Products.Remove(product);
                await context.SaveChangesAsync(HttpContext.RequestAborted);
                //Remove the cache entry as it is removed
                memoryCache.Remove(string.Format("product_{0}", id));
            }

            return RedirectToAction("Index");
        }

#if TESTING
        //
        // GET: /StoreManager/GetProductIdFromName
        // Note: Added for automated testing purpose. Application does not use this.
        [HttpGet]
        public IActionResult GetProductIdFromName(string productName)
        {
            var product = db.Products.Where(a => a.Title == productName).FirstOrDefault();

            if (product == null)
            {
                return HttpNotFound();
            }

            return new ContentResult { Content = product.ProductId.ToString(), ContentType = "text/plain" };
        }
#endif
    }
}
