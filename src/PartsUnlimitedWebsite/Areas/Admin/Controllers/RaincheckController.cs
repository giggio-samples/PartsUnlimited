﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Mvc;
using PartsUnlimited.Queries;

namespace PartsUnlimited.Areas.Admin.Controllers
{
    public class RaincheckController(IRaincheckQuery query) : AdminController
    {
        public async Task<IActionResult> Index()
        {
            var rainchecks = await query.GetAllAsync();

            return View(rainchecks);
        }
    }
}
