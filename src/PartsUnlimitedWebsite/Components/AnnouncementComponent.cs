﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using PartsUnlimited.Models;

namespace PartsUnlimited.Components
{
    [ViewComponent(Name = "Announcement")]
    public class AnnouncementComponent(IPartsUnlimitedContext context, IMemoryCache memoryCache) : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            if (!memoryCache.TryGetValue("announcementProduct", out Product announcementProduct))
            {
                announcementProduct = await GetLatestProduct();

                if (announcementProduct != null)
                {
                    memoryCache.Set("announcementProduct", announcementProduct, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
                }
            }

            return View(announcementProduct);
        }

        private Task<Product> GetLatestProduct()
        {
            var latestProduct = context.Products.OrderByDescending(a => a.Created).FirstOrDefault();
            if ((latestProduct != null) && ((latestProduct.Created - DateTime.UtcNow).TotalDays <= 2))
            {
                return Task.FromResult(latestProduct);
            }
            else
            {
                return Task.FromResult<Product>(null);
            }
        }
    }
}
