﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using PartsUnlimited.Models;

namespace PartsUnlimited.Components
{
    [ViewComponent(Name = "CategoryMenu")]
    public class CategoryMenuComponent(IPartsUnlimitedContext context, IMemoryCache memoryCache) : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            if (!memoryCache.TryGetValue("category", out List<Category> categoryList))
            {
                categoryList = await GetCategories();

                if (categoryList != null)
                {
                    memoryCache.Set("categoryList", categoryList, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
                }
            }
            return View(categoryList);
        }

        private async Task<List<Category>> GetCategories()
        {
            var category = await context.Categories.ToListAsync();
            return category;
        }
    }
}
