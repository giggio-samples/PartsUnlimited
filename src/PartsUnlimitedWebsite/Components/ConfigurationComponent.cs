﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Mvc;
using PartsUnlimited.WebsiteConfiguration;

namespace PartsUnlimited.Components
{
    [ViewComponent(Name = "Configuration")]
    public class ConfigurationComponent(IApplicationInsightsSettings appInsights) : ViewComponent
    {
        public Task<IViewComponentResult> InvokeAsync()
        {
            return Task.FromResult<IViewComponentResult>(View(appInsights));
        }
    }
}