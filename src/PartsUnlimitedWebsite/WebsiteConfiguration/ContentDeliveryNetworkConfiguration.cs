﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

namespace PartsUnlimited.WebsiteConfiguration
{
    public class ContentDeliveryNetworkConfiguration(IConfiguration config) : IContentDeliveryNetworkConfiguration
    {
        public string Images { get; } = config["images"];
        public ILookup<string, string> Scripts { get; } = config.GetSection("Scripts").ToLookup();
        public ILookup<string, string> Styles { get; } = config.GetSection("Styles").ToLookup();
    }
}