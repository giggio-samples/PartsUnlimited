﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

namespace PartsUnlimited.WebsiteConfiguration
{
    public class AzureMLFrequentlyBoughtTogetherConfig(IConfiguration config) : IAzureMLFrequentlyBoughtTogetherConfig
    {
        public string AccountKey { get; } = config["AccountKey"];
        public string ModelName { get; } = config["ModelName"];
    }
}