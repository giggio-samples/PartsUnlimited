﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using Microsoft.EntityFrameworkCore;
using PartsUnlimited.Models;

namespace PartsUnlimited.Search
{
    public class StringContainsProductSearch(IPartsUnlimitedContext context) : IProductSearch
    {
        public async Task<IEnumerable<Product>> Search(string query)
        {
            var lowercase_query = query.ToLower();

            var q = context.Products
                .Where(p => p.Title.ToLower().Contains(lowercase_query));

            return await q.ToListAsync();
        }
    }
}