﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using PartsUnlimited.Models;

namespace PartsUnlimited.ViewModels
{
    public class OrdersModel(IEnumerable<Order> orders, string username, DateTimeOffset startDate, DateTimeOffset endDate, string invalidOrderSearch, bool isAdminSearch)
    {
        public bool IsAdminSearch { get; } = isAdminSearch;
        public string InvalidOrderSearch { get; } = invalidOrderSearch;
        public IEnumerable<Order> Orders { get; } = orders;
        public string Username { get; } = username;
        public DateTimeOffset StartDate { get; } = startDate;
        public DateTimeOffset EndDate { get; } = endDate;
    }
}