﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using Microsoft.EntityFrameworkCore;
using PartsUnlimited.Models;

namespace PartsUnlimited.Queries
{
    public class RaincheckQuery(IPartsUnlimitedContext context) : IRaincheckQuery
    {
        public async Task<IEnumerable<Raincheck>> GetAllAsync()
        {
            var rainchecks = new List<Raincheck>();
            await foreach (var raincheck in context.RainChecks.AsAsyncEnumerable())
            {
                await FillRaincheckValuesAsync(raincheck);
                rainchecks.Add(raincheck);
            }
            return rainchecks;
        }

        public async Task<Raincheck> FindAsync(int id)
        {
            var raincheck = await context.RainChecks.FirstOrDefaultAsync();

            if (raincheck == null)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            await FillRaincheckValuesAsync(raincheck);

            return raincheck;
        }

        public async Task<int> AddAsync(Raincheck raincheck)
        {
            var addedRaincheck = context.RainChecks.Add(raincheck);

            await context.SaveChangesAsync(CancellationToken.None);

            return addedRaincheck.Entity.RaincheckId;
        }

        /// <summary>
        /// Lazy loading is not currently available with EF 7.0, so this loads the Store/Product/Category information
        /// </summary>
        private async Task FillRaincheckValuesAsync(Raincheck raincheck)
        {
            raincheck.IssuerStore = await context.Stores.FirstAsync(s => s.StoreId == raincheck.StoreId);
            raincheck.Product = await context.Products.FirstAsync(p => p.ProductId == raincheck.ProductId);
            raincheck.Product.Category = await context.Categories.FirstAsync(c => c.CategoryId == raincheck.Product.CategoryId);
        }
    }
}